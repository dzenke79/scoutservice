﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ScoutSluzba.Web.ViewModels
{
    public class LoginVM
    {
        [StringLength(100, ErrorMessage = "Username need to have min 3 characters", MinimumLength = 3)]
        public string Username { get; set; }

        [StringLength(100, ErrorMessage = "Password need to have min 4 characters", MinimumLength = 4)]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        public bool ZapamtiPassword { get; set; }
    }
}
