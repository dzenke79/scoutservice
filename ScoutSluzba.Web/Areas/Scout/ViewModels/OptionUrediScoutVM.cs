﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ScoutSluzba.Web.Areas.Scout.ViewModels
{
    public class OptionUrediScoutVM
    {
        public int ZaposlenikID { get; set; }
        public string Ime { get; set; }
        public string Prezime { get; set; }
        public string Mail { get; set; }
        public string Datum_rodjenja { get; set; }
        public string Kontakt_br { get; set; }
        public string Spol { get; set; }
        public List<SelectListItem> Grad { get; set; }
        public int GradID { get; set; }

    }
}
