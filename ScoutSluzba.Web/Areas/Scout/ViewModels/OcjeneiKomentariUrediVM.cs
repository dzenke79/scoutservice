﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ScoutSluzba.Web.ViewModels
{
    public class OcjeneiKomentariUrediVM
    {
        public int OcjenaKomentarID { get; set; }
        public int Ocjena { get; set; }
        public string Komentar { get; set; }
        public string Zaposlenik { get; set; }
        public string Igrac { get; set; }
    }
}
