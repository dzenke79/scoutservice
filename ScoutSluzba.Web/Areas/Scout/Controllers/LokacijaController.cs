﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Prodaja_Softvera_ver3.Web.Helper;
using ScoutSluzba.Data;
using ScoutSluzba.Data.EntityModels;
using ScoutSluzba.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ScoutSluzba.Web.Controllers
{
    [Area("Scout")]
    public class LokacijaController : Controller
    {
        private MyContext _context;

        public LokacijaController(MyContext context)
        {
            _context = context;
        }


        public IActionResult Index()
        {
            KorisnickiNalog korisnik = HttpContext.GetLogiraniKorisnik();
            if (korisnik.TipKorisnickogNaloga != "Zaposlenik")
            {
                ViewData["error_poruka"] = "Nemate pravo pristupa!";
                return RedirectToAction("Index", "Login");
            }

            var model = new LokacijaIndexVM
            {
                rows = _context.Lokacija
               .Select(x => new LokacijaIndexVM.Row
               {
                   LokacijaID = x.LokacijaID,
                   Naziv = x.Naziv,
                   Ulica = x.Ulica,
                   Adresa = x.Adresa,
                   GradID = x.GradID

               })
               .ToList()
            };

            return View(model);
        }


        public IActionResult Dodaj()
        {
            KorisnickiNalog korisnik = HttpContext.GetLogiraniKorisnik();
            if (korisnik.TipKorisnickogNaloga != "Zaposlenik")
            {
                ViewData["error_poruka"] = "Nemate pravo pristupa!";
                return RedirectToAction("Index", "Login");
            }

            var model = new LokacijaDodajVM();

            model.Grad = _context.Grad.Select(x => new SelectListItem
            {
                Value = x.GradID.ToString(),
                Text = x.Naziv
            }).ToList();



            return View(model);
        }

        //Treba Uraditi
        public IActionResult Snimi()
        {
            KorisnickiNalog korisnik = HttpContext.GetLogiraniKorisnik();
            if (korisnik.TipKorisnickogNaloga != "Zaposlenik")
            {
                ViewData["error_poruka"] = "Nemate pravo pristupa!";
                return RedirectToAction("Index", "Login");
            }

            var model = new LokacijaDodajVM();

            model.Grad = _context.Grad.Select(x => new SelectListItem
            {
                Value = x.GradID.ToString(),
                Text = x.Naziv
            }).ToList();



            return View(model);
        }

        public IActionResult Uredi(int id)
        {
            KorisnickiNalog korisnik = HttpContext.GetLogiraniKorisnik();
            if (korisnik.TipKorisnickogNaloga != "Zaposlenik")
            {
                ViewData["error_poruka"] = "Nemate pravo pristupa!";
                return RedirectToAction("Index", "Login");
            }

            var pronadji = _context.Lokacija.Find(id);
            var pronadjiGrad = _context.Grad.Where(x => x.GradID == pronadji.GradID).FirstOrDefault();

            var model = new LokacijaUrediVM
            {
                LokacijaID = id,
                Naziv = pronadji.Naziv,
                Ulica = pronadji.Ulica,
                Adresa = pronadji.Adresa,
                Grad = pronadjiGrad.Naziv
            };

            return View(model);
        }

        public IActionResult Obrisi(int Id)
        {
            KorisnickiNalog korisnik = HttpContext.GetLogiraniKorisnik();
            if (korisnik.TipKorisnickogNaloga != "Zaposlenik")
            {
                ViewData["error_poruka"] = "Nemate pravo pristupa!";
                return RedirectToAction("Index", "Login");
            }

            var z = _context.Lokacija.Find(Id);
            //var kn = _context.korisnickiNalog.Where(x => x.KorisnickiNalogID == z.KorisnickiNalogID).FirstOrDefault();
            if (z == null)
            {
                TempData["porukaError"] = "Greška pri brisanju Lokacije!!!";
            }
            else
            {
                _context.Remove(z);
                //_context.Remove(kn);
                _context.SaveChanges();
                TempData["porukaSuccess"] = "Uspjesno ste obrisali Lokacije!";
            }

            _context.Dispose();
            return RedirectToAction(nameof(Index));
        }
    }
}
