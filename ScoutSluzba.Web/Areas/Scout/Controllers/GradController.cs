﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Prodaja_Softvera_ver3.Web.Helper;
using ScoutSluzba.Data;
using ScoutSluzba.Data.EntityModels;
using ScoutSluzba.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ScoutSluzba.Web.Controllers
{
    [Area("Scout")]
    public class GradController : Controller
    {

        private MyContext _context;

        public GradController(MyContext context)
        {
            _context = context;
        }


        public IActionResult Index()
        {
            KorisnickiNalog korisnik = HttpContext.GetLogiraniKorisnik();
            if (korisnik.TipKorisnickogNaloga != "Zaposlenik")
            {
                ViewData["error_poruka"] = "Nemate pravo pristupa!";
                return RedirectToAction("Index", "Login");
            }

            var model = new GradIndexVM
            {
                rows = _context.Grad
               .Select(x => new GradIndexVM.Row
               {
                   GradID = x.GradID,
                   Naziv = x.Naziv,
                   PostanskiBr = x.PostanskiBr,
                   DrzavaID = x.DrzavaID

               })
               .ToList()
            };

            return View(model);
        }


        public IActionResult Dodaj()
        {
            KorisnickiNalog korisnik = HttpContext.GetLogiraniKorisnik();
            if (korisnik.TipKorisnickogNaloga != "Zaposlenik")
            {
                ViewData["error_poruka"] = "Nemate pravo pristupa!";
                return RedirectToAction("Index", "Login");
            }

            var model = new GradDodajVM();

            model.Drzava = _context.Drzava.Select(x => new SelectListItem
            {
                Value = x.DrzavaID.ToString(),
                Text = x.Naziv
            }).ToList();



            return View(model);
        }

        //Treba Uraditi
        public IActionResult Snimi()
        {
            KorisnickiNalog korisnik = HttpContext.GetLogiraniKorisnik();
            if (korisnik.TipKorisnickogNaloga != "Zaposlenik")
            {
                ViewData["error_poruka"] = "Nemate pravo pristupa!";
                return RedirectToAction("Index", "Login");
            }

            var model = new GradDodajVM();

            model.Drzava = _context.Drzava.Select(x => new SelectListItem
            {
                Value = x.DrzavaID.ToString(),
                Text = x.Naziv
            }).ToList();



            return View(model);
        }



        //Akcija: Uredi Grad
        public IActionResult Uredi(int id)
        {
            KorisnickiNalog korisnik = HttpContext.GetLogiraniKorisnik();
            if (korisnik.TipKorisnickogNaloga != "Zaposlenik")
            {
                ViewData["error_poruka"] = "Nemate pravo pristupa!";
                return RedirectToAction("Index", "Login");
            }

            var pronadji = _context.Grad.Find(id);

            var Drzava = _context.Drzava.Find(pronadji.DrzavaID);
            var model = new GradUrediVM
            {
                GradID = id,
                Naziv = pronadji.Naziv,
                PostanskiBr = pronadji.PostanskiBr,
                Drzava = pronadji.Drzava.Naziv
            };

            return View(model);
        }

        public IActionResult Obrisi(int Id)
        {
            KorisnickiNalog korisnik = HttpContext.GetLogiraniKorisnik();
            if (korisnik.TipKorisnickogNaloga != "Zaposlenik")
            {
                ViewData["error_poruka"] = "Nemate pravo pristupa!";
                return RedirectToAction("Index", "Login");
            }

            var z = _context.Grad.Find(Id);
            //var kn = _context.korisnickiNalog.Where(x => x.KorisnickiNalogID == z.KorisnickiNalogID).FirstOrDefault();
            if (z == null)
            {
                TempData["porukaError"] = "Greška pri brisanju Grada!!!";
            }
            else
            {
                _context.Remove(z);
                //_context.Remove(kn);
                _context.SaveChanges();
                TempData["porukaSuccess"] = "Uspjesno ste obrisali Grada!";
            }

            _context.Dispose();
            return RedirectToAction(nameof(Index));
        }

    }
}
