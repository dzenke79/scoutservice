﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ScoutSluzba.Web.ViewModels
{
    public class LigaDodajVM
    {
        public int LigaID { get; set; }
        public string Naziv { get; set; }
        public string Sampion { get; set; }
        public string BrTimova { get; set; }
    }
}
