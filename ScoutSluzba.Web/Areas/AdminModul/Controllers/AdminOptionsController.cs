﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Prodaja_Softvera_ver3.Web.Helper;
using ScoutSluzba.Data;
using ScoutSluzba.Data.EntityModels;
using ScoutSluzba.Web.Areas.AdminModul.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ScoutSluzba.Web.Areas.AdminModul.Controllers
{
    [Area("AdminModul")]
    public class AdminOptionsController : Controller
    {
        private MyContext _context;
        public AdminOptionsController(MyContext context)
        {
            _context = context;
        }


        public IActionResult Index(int id)
        {
            KorisnickiNalog korisnik = HttpContext.GetLogiraniKorisnik();
            if (korisnik.TipKorisnickogNaloga != "Administartor")
            {
                ViewData["error_poruka"] = "Nemate pravo pristupa!";
                return RedirectToAction("Index", "Login");
            }


            var pronadji = _context.Zaposlenik.Find(id);
            var pronadjiGrad = _context.Grad.Find(pronadji.GradID);

            var model = new AdminOptionsVM
            {
                ZaposlenikID = pronadji.ZaposlenikID,
                Ime = pronadji.Ime,
                Prezime = pronadji.Prezime,
                Mail = pronadji.Email,
                Kontakt_br = pronadji.Kontakt_br,
                Datum_rodjenja = pronadji.Datum_rodjenja,
                Spol = pronadji.Spol,
                Grad = pronadjiGrad.Naziv
            };
            return View(model);
        }


        public IActionResult Uredi(int id)
        {
            KorisnickiNalog korisnik = HttpContext.GetLogiraniKorisnik();
            if (korisnik.TipKorisnickogNaloga != "Administartor")
            {
                ViewData["error_poruka"] = "Nemate pravo pristupa!";
                return RedirectToAction("Index", "Login");
            }

            var pronadji = _context.Zaposlenik.Find(id);
            var pronadjiGrad = _context.Grad.Find(pronadji.GradID);

            var model = new AdminOptionUrediVM
            {
                ZaposlenikID = pronadji.ZaposlenikID,
                Ime = pronadji.Ime,
                Prezime = pronadji.Prezime,
                Mail = pronadji.Email,
                Kontakt_br = pronadji.Kontakt_br,
                Datum_rodjenja = pronadji.Datum_rodjenja,
                Spol = pronadji.Spol
            };

            model.Grad = _context.Grad.Select(x => new SelectListItem
            {
                Value = x.GradID.ToString(),
                Text = x.Naziv
            }).ToList();

            return View(model);

        }
    }
}
