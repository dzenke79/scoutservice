﻿using Microsoft.AspNetCore.Mvc;
using ScoutSluzba.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ScoutSluzba.Web.Controllers
{
    public class SingInController : Controller
    {
        private MyContext _context;

        public SingInController(MyContext context)
        {
            _context = context;
        }


        public IActionResult Index()
        {
            return View();
        }
    }
}
