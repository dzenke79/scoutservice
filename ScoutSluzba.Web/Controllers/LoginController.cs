﻿using Microsoft.AspNetCore.Mvc;
using Prodaja_Softvera_ver3.Web.Helper;
using ScoutSluzba.Data;
using ScoutSluzba.Data.EntityModels;
using ScoutSluzba.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ScoutSluzba.Web.Controllers
{
    public class LoginController : Controller
    {
        private MyContext _context;

        public LoginController(MyContext context)
        {
            _context = context;
        }


        public IActionResult Index()
        {
            //return View(new LoginVM()
            //{
            //    ZapamtiPassword = true,
            //});

            return View();
        }

        public IActionResult Login(LoginVM input)
        {
            KorisnickiNalog korisnik = _context.korisnickiNalog
                .Where(x => x.KorisnickoIme == input.Username && x.Lozinka == input.Password).SingleOrDefault();

            Greska g = new Greska();

            if (korisnik == null)
            {
                g.greska = "pogrešan username ili password";
                g.VrijemeGreske = DateTime.Now;

                _context.Greska.Add(g);
                _context.SaveChanges();

                TempData["error_poruka"] = "Wrong Username or Password";
                return RedirectToAction("Index", input);
            }

            HttpContext.SetLogiraniKorisnik(korisnik, input.ZapamtiPassword);

            bool isAdministrator = false, isZaposlenik = false, isKlijent = false;

            KorisnickiNalog k = _context.korisnickiNalog.Where(x => x.KorisnickiNalogID == korisnik.KorisnickiNalogID).FirstOrDefault();

            if (k.TipKorisnickogNaloga == "Administartor")
            {
                isAdministrator = true;
                return RedirectToAction("Index", "Igraci", new { area = "AdminModul" });
            }

            if (k.TipKorisnickogNaloga == "Zaposlenik")
            {
                isZaposlenik = true;
                return RedirectToAction("Index", "Drzava", new { area = "Scout" });
            }


            if (k.TipKorisnickogNaloga == "Klijent")
            {
                //var klijent = _db.Klijent.Where(x => x.KorisnickiNalogID == k.ID).SingleOrDefault();

                //Random generator = new Random();
                //int r = generator.Next(100000, 1000000);
                //Kod kod = new Kod
                //{
                //    kod = r,
                //    IsValid = true,
                //    VrijemeSlanja = DateTime.Now
                //};

                //var client = new Client(creds: new Nexmo.Api.Request.Credentials
                //{
                //    ApiKey = "f372b717",
                //    ApiSecret = "gJ6bpKeFXbr5xqIJ"
                //});
                //var results = client.SMS.Send(request: new SMS.SMSRequest
                //{
                //    from = "Prodaja softvera",
                //    to = "38761843304" /*+klijent.Kontakt_broj*/,
                //    text = "Vas aktivacijski kod: " + r.ToString()
                //});

                isKlijent = true;
                return RedirectToAction("Index", "SoftvareKlijent", new { area = "KlijentModul" });

                //return RedirectToAction("PotvrdiIndex");
            }

            return RedirectToAction("Index", "Home");
        }



        public IActionResult PotvrdiIndex(int kod)
        {
            return View();
        }

        public IActionResult Potvrdi(int kod)
        {
            //List<int> kodovi = _context.Kod.Where(t => t.kod == kod).Select(n => n.ID).ToList();

            //foreach (var item in kodovi)
            //{
            //    Kod k = _context.Kod.Find(item);
            //    if (k.IsValid != true && k.VrijemeSlanja.Hour - DateTime.Now.Hour > 5 && k.kod != kod)
            //    {
            //        TempData["act_poruka"] = ("Kod nije aktiviran. Pokušajte ponovo.");
            //        return RedirectToAction("Login");
            //    }

            //    k.IsValid = false;
            //}

            return RedirectToAction("Index", "SoftvareKlijent", new { area = "KlijentModul" });
        }

        public IActionResult Logout()
        {

            HttpContext.DeleteLogiraniKorisnik();
            return RedirectToAction("Index");
        }
    }
}
