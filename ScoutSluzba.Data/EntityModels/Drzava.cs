﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ScoutSluzba.Data.EntityModels
{
    public class Drzava
    {
        public int DrzavaID { get; set; }
        public string Naziv { get; set; }
        public string Oznaka { get; set; }
    }
}
