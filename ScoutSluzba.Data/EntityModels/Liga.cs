﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ScoutSluzba.Data.EntityModels
{
    public class Liga
    {
        public int LigaID { get; set; }
        public string Naziv { get; set; }
        public string Sampion { get; set; }
        public string BrTimova { get; set; }
    }
}
