﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ScoutSluzba.Data.Migrations
{
    public partial class migi112 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "imgurl",
                table: "Igrac",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "imgurl",
                table: "Igrac");
        }
    }
}
