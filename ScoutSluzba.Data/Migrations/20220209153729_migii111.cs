﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ScoutSluzba.Data.Migrations
{
    public partial class migii111 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Slika",
                table: "Team",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Slika",
                table: "Team");
        }
    }
}
